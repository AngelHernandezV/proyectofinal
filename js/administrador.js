import { initializeApp } from "https://www.gstatic.com/firebasejs/9.13.0/firebase-app.js";
import { getAuth, signInWithEmailAndPassword, createUserWithEmailAndPassword, signOut } from "https://www.gstatic.com/firebasejs/9.13.0/firebase-auth.js";
import { getFirestore, doc, getDoc, getDocs, collection }
    from "https://www.gstatic.com/firebasejs/9.13.0/firebase-firestore.js";
import { getDatabase, onValue, ref, set, child, get, update, remove }
    from "https://www.gstatic.com/firebasejs/9.13.0/firebase-database.js";
import { getStorage, ref as refS, uploadBytes, getDownloadURL }
    from "https://www.gstatic.com/firebasejs/9.13.0/firebase-storage.js";
    
  // Your web app's Firebase configuration
  const firebaseConfig = {
    apiKey: "AIzaSyCVvKQl0t0u6d5fuXthxNr2PGjj0SrwkOA",
    authDomain: "finalweb-69f91.firebaseapp.com",
    databaseURL: "https://finalweb-69f91-default-rtdb.firebaseio.com",
    projectId: "finalweb-69f91",
    storageBucket: "finalweb-69f91.appspot.com",
    messagingSenderId: "215674283336",
    appId: "1:215674283336:web:29be335446e759a8afff7d"
  };

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const db = getDatabase();
var btnAgregar = document.getElementById('btnAgregar');
var btnConsultar = document.getElementById('btnConsultar');
var btnActualizar = document.getElementById('btnActualizar');
var btnBorrar = document.getElementById('btnBorrar');
var btnTodos = document.getElementById('btnTodos');
var btnLimpiar = document.getElementById('btnLimpiar');

var btnVerImagen = document.getElementById('btnVerImagen');
//imagen
var archivos = document.getElementById('archivo');

//Insertar variables inputs
var codigo = 0;
var nombre = "";
var descripcion = "";
var precio = 0.0;
var url = "";
var archivo = "";

function leerInputs() {
    codigo = document.getElementById('codigo').value;
    nombre = document.getElementById('nombre').value;
    descripcion = document.getElementById('descripcion').value;
    precio = document.getElementById('precio').value;
    //imagen
    archivo = document.getElementById('imgNombre').value;
    url = document.getElementById('url').value;

    //alert("  Matricula  " + matricula + "  Nombre  " + nombre + "  Carrera  " + carrera + "  Genero  " + genero);
}


function insertarDatos() {
    leerInputs();
    set(ref(db, 'Productos/' + codigo), {
        nombre: nombre,
        descripcion: descripcion,
        precio: precio,
        archivo: archivo,
        url: url
    }).then((res) => {
        alert("Se Inserto con exito")
    }).catch((error) => {
        alert("Surgio un error " + error)
    });
}

function mostrarDatos() {
    leerInputs();
    const dbref = ref(db);
    get(child(dbref, 'Productos/' + codigo)).then((snapshot) => {
        if (snapshot.exists()) {
            nombre = snapshot.val().nombre;
            descripcion = snapshot.val().descripcion;
            precio = snapshot.val().precio;
            archivo = snapshot.val().archivo;
            url = snapshot.val().url;
            alert(url + ":" + archivo);
            escribirInputs();
        }
        else {
            alert("No se encontro el registro ");
        }
    }).catch((error) => {
        alert("Surgio un error " + error);
    });

}

function actualizar() {
    leerInputs();
    update(ref(db, 'Productos/' + codigo), {
        nombre: nombre,
        descripcion: descripcion,
        precio: precio,
        archivo: archivo,
        url: url
    }).then(() => {
        alert("Se realizo actualizacion");
        mostrarAlumnos();
    }).catch(() => {
        alert("Causo Error " + error);
    });
}

function borrar() {
    leerInputs();
    remove(ref(db, 'Productos/' + codigo)).then(() => {
        alert("Se borro con exito");
        mostrarAlumnos();
    }).catch(() => {
        alert("Causo Error " + error);
    });
}

function mostrarAlumnos() {
    const db = getDatabase();
    const dbRef = ref(db, 'Equipos');

    onValue(dbRef, (snapshot) => {
        lista.innerHTML = ""
        snapshot.forEach((childSnapshot) => {

            const childKey = childSnapshot.key;
            const childData = childSnapshot.val();

            lista.innerHTML = "<div class='campo'>" + lista.innerHTML + childKey + " | " + childData.nombre + " | " + childData.descripcion + " | " + childData.precio + " | " + childData.archivo + " | " + childData.url +
                "<br></div>";
            console.log(childKey + ":");
            console.log(childData.nombre)
        });
    }, {
        onlyOnce: true
    });
}



function escribirInputs() {
    document.getElementById('codigo').value = codigo;
    document.getElementById('nombre').value = nombre;
    document.getElementById('descripcion').value = descripcion;
    document.getElementById('precio').value = precio;
    document.getElementById('imgNombre').value = archivo;
    document.getElementById('url').value = url;
}


function CargarImagen() {
    const file = event.target.files[0]
    const name = event.target.files[0].name;
    document.getElementById('imgNombre').value = name;
    const storage = getStorage();
    const storageRef = refS(storage, 'imagenes/' + name);
    uploadBytes(storageRef, file).then((snapshot) => {
        //descargarImagen(name);
        alert("Se cargo la imagen")
    });
}

function descargarImagen() {
    archivo = document.getElementById('imgNombre').value;
    alert("el archivo es:" + 'imagenes/' + archivo)
    const storage = getStorage();
    const starsRef = refS(storage, 'imagenes/' + archivo);

    // Get the download URL
    getDownloadURL(starsRef)
        .then((url) => {
            // Insert url into an <img> tag to "download"
            // document.getElementById('imagen').src=url;
            console.log(url);

            document.getElementById('imagen').src = url;
            document.getElementById('url').value = url;

        })
        .catch((error) => {
            // A full list of error codes is available at
            // https://firebase.google.com/docs/storage/web/handle-errors
            switch (error.code) {
                case 'storage/object-not-found':
                    console.log("No se encontro la imagen")
                    break;
                case 'storage/unauthorized':
                    console.log("NO Tiene permisos para accesar imagen")
                    break;
                case 'storage/canceled':
                    console.log("se cancelo la subida");
                    break;
                // ...
                case 'storage/unknown':
                    // Unknown error occurred, inspect the server response
                    break;

            }
        });

}

function limpiar() {
    lista.innerHTML = "";
    codigo = "";
    nombre = "";
    descripcion = "";
    precio = "";
    archivo = "";
    url = "";
    escribirInputs();
}

//btnAgregar.addEventListener('click', leerInputs);
btnAgregar.addEventListener('click', insertarDatos);
btnConsultar.addEventListener('click', mostrarDatos);
btnActualizar.addEventListener('click', actualizar);
btnBorrar.addEventListener('click', borrar);
btnTodos.addEventListener('click', mostrarAlumnos);
btnLimpiar.addEventListener('click', limpiar);

archivos.addEventListener('change', CargarImagen);
btnVerImagen.addEventListener('click', descargarImagen);